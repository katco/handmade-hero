;(load "~/quicklisp/setup.lisp")
;(ql:quickload "cl-utilities")
;(load "scraper.lisp")

;; Set production values for performance.
(sb-ext:restrict-compiler-policy 'debug 0)
(sb-ext:restrict-compiler-policy 'safety 0)
(sb-ext:describe-compiler-policy)

(ql:quickload "handmade-hero")
(sb-ext:save-lisp-and-die
 "handmade-hero"
 :purify t
 :compression 9
 :executable t
 :toplevel #'handmade-hero:main)

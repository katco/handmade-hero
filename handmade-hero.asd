;;;; handmade-hero.asd

(asdf:defsystem #:handmade-hero
  :description "A common lisp implementation of Casey Muratori's Handmade Hero."
  :author "Katherine Cox-Buday <cox.katherine.e@gmail.com>"
  :license "MIT"
  :serial t
  :entry-point "handmade-hero:main"
  :components ((:file "package")
               (:file "handmade-hero"))
  :depends-on (#:sdl2
               #:cffi))

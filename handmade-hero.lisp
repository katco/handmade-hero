;;;; handmade-hero.lisp

(in-package #:handmade-hero)

;;; "handmade-hero" goes here. Hacks and glory await!

(defconstant +initial-win-height+ 1024)
(defconstant +initial-win-width+ 760)

(defclass offscreen-buffer ()
  ((texture :initform nil)
   memory
   (width :type integer)
   (height :type integer)
   (pitch :type integer)))

(defparameter *global-backbuffer* (make-instance 'offscreen-buffer))
(defparameter *controllers* '())

(defun main()
  (sdl2:with-init (:everything)
    (multiple-value-bind (win renderer)
        (sdl2:create-window-and-renderer +initial-win-height+ +initial-win-width+
                                         '(:shown :opengl :resizable))
      (sdl2:set-window-title win "Handmade Hero")

      (let ((*global-backbuffer* (make-instance 'offscreen-buffer))
            (x-offset 0)
            (y-offset 0)
            (*controllers* (open-game-controllers)))

        ;; Open our audio device.
        (init-audio 4800 4096)

        ;; Initialize our texture so the window can display it when it
        ;; first comes up.
        (resize-texture *global-backbuffer* renderer +initial-win-height+ +initial-win-width+)

        ;; Main event loop
        (sdl2:with-event-loop (:method :poll)

          (:windowevent (:event event-type)
                        (cond
                          ((equal sdl2-ffi:+sdl-windowevent-exposed+ event-type)
                           (update-window *global-backbuffer* renderer))))

          (:keydown (:keysym keysym)
                    (let ((coords (handle-key-press keysym)))
                      ;; Because lisp has unbounded numbers (normally a good
                      ;; thing), we need to cap the increments to replicate a c
                      ;; integer.
                      (setf x-offset
                            (ldb (byte 8 0) (incf x-offset (* 8 (car coords)))))
                      (setf y-offset
                            (ldb (byte 8 0) (incf y-offset (* 8 (cadr coords)))))))

          (:keyup (:keysym keysym)
                  (format t "Key-down: ~a~%" (handle-key-press keysym :up? t)))

          (:mousemotion (:x x :y y :xrel xrel :yrel yrel :state state)
                        (format t "Mouse motion abs(rel): ~a (~a), ~a (~a)~%Mouse state: ~a~%"
                                x xrel y yrel state))

          (:controlleraxismotion (:which controller-id
                                  :axis axis-id
                                  :value value)
                                 (format t
                                         "Controller axis motion: Controller: ~a, Axis: ~a, Value: ~a~%"
                                         controller-id axis-id value))

          (:controllerbuttondown (:which controller-id)
                                 (let ((h (cdr (assoc controller-id haptic))))
                                   (when h
                                     (sdl2:rumble-play h 1.0 100))))

          (:idle ()
                 (render-weird-gradient *global-backbuffer* x-offset y-offset)
                 (update-window *global-backbuffer* renderer))

          (:quit () t))

        ;; Close all the game controllers.
        (mapcar #'game-controller-close *controllers*)))))

(defun handle-key-press (keysym &key up?)
  (let ((scancode (sdl2:scancode-value keysym)))
    (cond
      ((sdl2:scancode= scancode :scancode-w) '(0 1))
      ((sdl2:scancode= scancode :scancode-a) '(-1 0))
      ((sdl2:scancode= scancode :scancode-s) '(0 -1))
      ((sdl2:scancode= scancode :scancode-d) '(1 0))
      (t '(0 0)))))

(defclass game-controller ()
  (controller
   joystick
   (haptic
    :initform nil)))

(defmethod game-controller-close ((controller game-controller))
  (sdl2:game-controller-close (slot-value controller 'controller))
  (when (slot-value controller 'controller)
    (sdl2:haptic-close (slot-value controller 'controller))))

(defun open-game-controllers ()
  (loop for device-idx below (sdl2:joystick-count)
        if (sdl2:game-controller-p device-idx)
          collect (let* ((game-controller (make-instance 'game-controller))
                         (controller (sdl2:game-controller-open device-idx))
                         (joystick (sdl2:game-controller-get-joystick controller)))
                    (setf (slot-value game-controller 'controller) controller
                          (slot-value game-controller 'joystick) joystick)
                    (when (sdl2:joystick-is-haptic-p joystick)
                      (setf (slot-value game-controller 'haptic) (sdl2:haptic-open-from-joystick joystick))
                      (sdl2:rumble-init (slot-value game-controller 'haptic)))
                    game-controller)))

(defun resize-texture (buffer renderer width height)
  "Resize our textures to fit the new window dimensions."

  ;; NOTE(kate): Uncomment to use a block of memory not allocated by SDL2
  ;;(when (slot-value *global-backbuffer* 'memory) (cffi:foreign-free (slot-value *global-backbuffer* 'memory)))
  (when (slot-value buffer 'texture) (sdl2:destroy-texture (slot-value buffer 'texture)))

  (setf (slot-value buffer 'width) width
        (slot-value buffer 'height) height
        (slot-value buffer 'texture) (sdl2:create-texture renderer
                                                          :argb8888
                                                          :streaming
                                                          width
                                                          height)
        ;; NOTE(kate): Uncomment to use a block of memory not allocated by SDL2
        ;;(slot-value buffer 'memory) (cffi:foreign-alloc :char :count (* width height +bytes-per-pixel+))
        (values
         (slot-value buffer 'memory)
         (slot-value buffer 'pitch))
        (sdl2:lock-texture (slot-value buffer 'texture))))


(defun update-window (buffer renderer)
  "Applies the bitmap to the texture and renders it."
  (let ((bytes-per-pixel 4))
    (sdl2:update-texture (slot-value buffer 'texture)
                         (slot-value buffer 'memory)
                         :width (* (slot-value buffer 'width) bytes-per-pixel)))
  (sdl2:render-copy renderer (slot-value buffer 'texture))
  (sdl2:render-present renderer))

(defun render-weird-gradient (buffer blue-offset green-offset)
  "Renders a blue-to-green gradient onto our bitmap."
  (declare (type integer blue-offset green-offset))
  (declare (type offscreen-buffer buffer))
  (loop with row = (slot-value buffer 'memory)
        for y below (slot-value buffer 'height)
        for green = (+ y green-offset)
        for pixel = row
        do (loop for x below (slot-value buffer 'width)
                 for blue = (+ x blue-offset)
                 do (setf (cffi:mem-ref pixel :int32) (logior (ash green 8) blue))
                    (cffi:incf-pointer pixel 4))
           (cffi:incf-pointer row (slot-value buffer 'pitch))))

(defun audio-callback (user-data audio-data length)
  (format t "In audio callback~%"))

(defun init-audio (samples-per-second buffer-size)
  ;; (sdl2-mixer:open-audio sdl2-ffi:+audio-s16lsb+
  ;;                        buffer-size
  ;;                        :freq samples-per-second
  ;;                        :callback-fn #'audio-callback)
  ;; (format t "Initialized audio device at frequency ~a Hz, ~a Channels~% " samples-per-second 2)
  ;; (sdl2:pause-audio 0)
  )
